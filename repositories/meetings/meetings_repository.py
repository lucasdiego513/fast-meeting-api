"""Meetings repository module."""
from sqlalchemy.orm import Session
from domain.models import Meetings
from domain.schemas import MeetingsOutput, MeetingsInput


def get_meeting_by_key(session, key):
    """Get a meeting by key"""
    return session.query(Meetings).filter(Meetings.key == key).first()


def get_all(session: Session) -> list[MeetingsOutput]:
    """Get all meetings"""
    meetings = session.query(Meetings).all()

    return [MeetingsOutput.from_orm(meeting) for meeting in meetings]


def add_meeting(session: Session, body: MeetingsInput) -> MeetingsOutput:
    """Add a new meeting"""
    meeting = Meetings(**body.dict())

    session.add(meeting)
    session.flush()

    return MeetingsOutput.from_orm(meeting)
