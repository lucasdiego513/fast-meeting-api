"""Repository for rooms"""
from sqlalchemy.orm import Session
from domain.models import Rooms
from domain.schemas import RoomsOutput, RoomsInput


def get_room_by_key(session, key):
    """Get a room by key"""
    return session.query(Rooms).filter(Rooms.key == key).first()


def get_all(session: Session) -> list[RoomsOutput]:
    """Get all rooms"""
    rooms = session.query(Rooms).all()

    return [RoomsOutput.from_orm(room) for room in rooms]


def add_room(session: Session, body: RoomsInput) -> RoomsOutput:
    """Add a new room"""
    room = Rooms(**body.dict())

    session.add(room)
    session.flush()

    return RoomsOutput.from_orm(room)
