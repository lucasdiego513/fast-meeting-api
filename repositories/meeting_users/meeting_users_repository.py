"""Meeting users repository"""
from sqlalchemy.orm import Session
from domain.models import MeetingUsers, Users
from domain.schemas import MeetingUsersOutput, MeetingUsersInput


def get_all(session: Session) -> list[MeetingUsersOutput]:
    """Get all meeting users (filter unique meetings)"""
    meeting_users = session.query(MeetingUsers).distinct(MeetingUsers.meeting_key).all()

    return [
        MeetingUsersOutput(
            meeting=meeting_user.meeting,
            users=get_meeting_users(
                session, meeting_user.meeting_key)) for meeting_user in meeting_users
    ]


def add_meeting_user(session: Session, body: MeetingUsersInput) -> MeetingUsersOutput:
    """Add a new meeting user"""
    meeting_user = MeetingUsers(**body.dict())

    session.add(meeting_user)
    session.flush()

    # Get all users for the meeting
    users = get_meeting_users(session, meeting_user.meeting_key)

    # Construct the MeetingUsersOutput object
    meeting_users_output = MeetingUsersOutput(meeting=meeting_user.meeting, users=users)

    return meeting_users_output


def get_meeting_users(session: Session, meeting_key: str) -> list[Users]:
    """Get all users for a meeting"""
    users = session.query(Users).join(MeetingUsers) \
        .filter(MeetingUsers.meeting_key == meeting_key).all()

    return users
