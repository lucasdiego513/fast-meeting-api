"""Users repository module."""
from sqlalchemy.orm import Session
from sqlalchemy import or_
from domain.models import Users
from domain.schemas import UsersOutput, UsersInput


def get_user_by_key_or_name(session, key, name):
    """Get a user by key and name"""
    return session.query(Users).filter(or_(Users.key == key, Users.name == name)).first()


def get_all(session: Session) -> list[UsersOutput]:
    """Get all users"""
    users = session.query(Users).all()

    return [UsersOutput.from_orm(user) for user in users]


def add_user(session: Session, body: UsersInput) -> UsersOutput:
    """Add a new user"""
    user = Users(**body.dict())

    session.add(user)
    session.flush()

    return UsersOutput.from_orm(user)
