"""Application initializing function and printing"""

from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
import uvicorn
from config.database import db
from config.server import base_url, port
from config import application, squid_url, portal_id, environment
from config.middlewares import middlewares
from domain.errors import base_errors
from controllers import routes, tags
from utils.valuable import valuable

DESCRIPTION = """
This is a SUBWEB service dedicated to 🚀A Fast Meeting API Backend🚀

Developed by [LCCV/UFAL](https://lccv.ufal.br/).
"""

app = FastAPI(
    title=application["name"],
    description=DESCRIPTION,
    version="0.0.1",
    contact={
        "name": "SUBWEB support",
        "url": "https://dev.subweb.com.br",
        "email": "lucaslino@lccv.ufal.br",
    },
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},
    responses=base_errors,
)

for middleware in middlewares:
    app.middleware(middleware.type)(middleware(app))

for router in routes:
    app.include_router(router)

openapi_schema = get_openapi(
    title=app.title,
    version=app.version,
    description=app.description,
    servers=[{"url": base_url}],
    routes=app.routes,
    tags=tags)

api = FastAPI(openapi_url=None)
api.openapi = openapi_schema
api.mount(base_url, app)

print(f'''
Service {application["id"]} is available on port {port}:

  Environment variables:

    -> Database:
        USER_DB: {db["user"]}
        HOST_DB: {db["host"]}
        PORT_DB: {db["port"]}
        SCHEMA_DB: {db["schema"]}
        NAME_DB: {db["name"]}
        IS_PASSWORD_DEFINED: {valuable(db["password"])}

    -> Server:
        SUB_DIR: {base_url}
        SERVER_PORT: {str(port)}

    -> Custom:
        APP_NAME: {application["name"]}
        SQUID_BASE_PATH: {squid_url}
        SUBWEB_ENVIRONMENT: {environment}
        PORTAL_ID: {portal_id}

  Docs (Swagger) available on: http://localhost:{port}{base_url or ""}{app.docs_url}.
''')

reload = environment.lower() != 'prd'

if __name__ == "__main__":
    uvicorn.run('main:api',
                host='0.0.0.0',
                port=port,
                reload=reload)
