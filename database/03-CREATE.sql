-- Create the users table
create table app_fastmeet.users (
    user_cd_users varchar(5) not null,
    user_nm_users varchar not null,
    user_email_users varchar not null,
    user_nm_username varchar not null,
    user_cd_cpf varchar not null,
    user_nr_phone varchar,
    constraint pk_user primary key (user_cd_users)
);

comment on table app_fastmeet.users is 'Users table';
comment on column app_fastmeet.users.user_cd_users is 'Petrobras key of the user';
comment on column app_fastmeet.users.user_nm_users is 'Name of the user';
comment on column app_fastmeet.users.user_email_users is 'Email of the user';
comment on column app_fastmeet.users.user_nm_username is 'Username of the user';
comment on column app_fastmeet.users.user_cd_cpf is 'CPF of the user';
comment on column app_fastmeet.users.user_nr_phone is 'Phone number of the user';


-- Create the roles table
create table app_fastmeet.roles (
    role_cd_roles varchar(5) not null,
    role_nm_roles varchar not null,
    role_ds_roles varchar not null,
    constraint pk_roles primary key (role_cd_roles)
);

comment on table app_fastmeet.roles is 'Roles table';
comment on column app_fastmeet.roles.role_cd_roles is 'Key of the role';
comment on column app_fastmeet.roles.role_nm_roles is 'Name of the role';
comment on column app_fastmeet.roles.role_ds_roles is 'Description of the role';


-- Create the user_roles table
create table app_fastmeet.user_roles (
    user_cd_users varchar(5) not null,
    role_cd_roles varchar(5) not null,
    constraint pk_usro primary key (user_cd_users, role_cd_roles),
    constraint fk_usro_user foreign key (user_cd_users) references app_fastmeet.users (user_cd_users),
    constraint fk_usro_role foreign key (role_cd_roles) references app_fastmeet.roles (role_cd_roles)
);

comment on table app_fastmeet.user_roles is 'User roles table';
comment on column app_fastmeet.user_roles.user_cd_users is 'Petrobras key of the user';
comment on column app_fastmeet.user_roles.role_cd_roles is 'Key of the role';


-- Create rooms table
create table app_fastmeet.rooms (
    room_cd_rooms varchar(5) not null,
    room_nm_rooms varchar not null,
    room_ds_rooms varchar,
    room_nr_capacity integer not null,
    constraint pk_rooms primary key (room_cd_rooms)
);

comment on table app_fastmeet.rooms is 'Rooms table';
comment on column app_fastmeet.rooms.room_cd_rooms is 'Key of the room';
comment on column app_fastmeet.rooms.room_nm_rooms is 'Name of the room';
comment on column app_fastmeet.rooms.room_ds_rooms is 'Description of the room';
comment on column app_fastmeet.rooms.room_nr_capacity is 'Capacity of the room';


-- Create the meetings table
create table app_fastmeet.meetings (
    meet_cd_meetings varchar(5) not null,
    meet_nm_subject varchar not null,
    meet_ds_meetings varchar,
    meet_df_start timestamp not null,
    meet_df_end timestamp not null,
    room_cd_rooms varchar(5) not null,
    constraint pk_meet primary key (meet_cd_meetings),
    constraint fk_meet_room foreign key (room_cd_rooms) references app_fastmeet.rooms (room_cd_rooms)
);

comment on table app_fastmeet.meetings is 'Meetings table';
comment on column app_fastmeet.meetings.meet_cd_meetings is 'Key of the meeting';
comment on column app_fastmeet.meetings.meet_nm_subject is 'Subject of the meeting';
comment on column app_fastmeet.meetings.meet_ds_meetings is 'Description of the meeting';
comment on column app_fastmeet.meetings.meet_df_start is 'Start date and time of the meeting';
comment on column app_fastmeet.meetings.meet_df_end is 'End date and time of the meeting';
comment on column app_fastmeet.meetings.room_cd_rooms is 'Key of the room';


-- Create the meeting_users table
create table app_fastmeet.meeting_users (
    meet_cd_meetings varchar(5) not null,
    user_cd_users varchar(5) not null,
    constraint pk_meus primary key (meet_cd_meetings, user_cd_users),
    constraint fk_meus_meet foreign key (meet_cd_meetings) references app_fastmeet.meetings (meet_cd_meetings),
    constraint fk_meus_user foreign key (user_cd_users) references app_fastmeet.users (user_cd_users)
);

comment on table app_fastmeet.meeting_users is 'Meeting users table';
comment on column app_fastmeet.meeting_users.meet_cd_meetings is 'Key of the meeting';
comment on column app_fastmeet.meeting_users.user_cd_users is 'Petrobras key of the user';
