"""Export all middlewares from this module."""
from .exception import ExceptionCollector

middlewares = [
    ExceptionCollector,
]
