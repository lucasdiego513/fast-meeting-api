# pylint: disable=invalid-name
"""Env var importing package"""
import os
import logging
from dotenv import load_dotenv

load_dotenv()

environment = os.getenv('SUBWEB_ENVIRONMENT') or 'dev'

application = {
    "id": os.getenv('APP_ID') or 'sbwb-fast-meeting-backend',
    "name": os.getenv('APP_NAME') or 'SUBWEB FAST MEETING API',
    "token": os.getenv('APP_TOKEN') or '',
    "version": os.getenv('APP_VERSION') or '0.0.0',
}

squid_url = os.getenv('SQUID_BASE_PATH') or ''

enable_mock = False  # type: ignore
if os.getenv('ENABLE_MOCK') is not None:
    enable_mock = os.getenv('ENABLE_MOCK').upper() == "TRUE"  # type: ignore

debug_mode = False  # type: ignore
if os.getenv('DEBUG_MODE') is not None:
    debug_mode = os.getenv('DEBUG_MODE').upper() == "TRUE"  # type:ignore
    disable_debug_query = os.getenv('DISABLE_DEBUG_QUERY', "FALSE") == "TRUE"
    if debug_mode:
        logging.getLogger().setLevel(logging.INFO)
    if debug_mode and not disable_debug_query:
        logging.basicConfig()
        logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

portal_id = os.getenv('PORTAL_ID') or 'portal_subweb'
