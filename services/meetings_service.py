"""Service layer for meetings"""
from sqlalchemy.orm import Session
from repositories import meetings as meetings_repository
from domain.schemas.meetings import MeetingsOutput, MeetingsInput
from domain.errors import ResourceAlreadyExists


def get_all_meetings(session: Session) -> list[MeetingsOutput]:
    """Get all registered meetings"""
    return meetings_repository.get_all(session)


def create_meeting(session: Session, body: MeetingsInput) -> MeetingsOutput:
    """Create a new meeting"""

    # Check if the meeting already exists by the key
    existing_meeting = meetings_repository.get_meeting_by_key(session, body.key)
    if existing_meeting:
        raise ResourceAlreadyExists(f"{existing_meeting.key}")

    # Check if the room is occupied at the specified time

    return meetings_repository.add_meeting(session, body)
