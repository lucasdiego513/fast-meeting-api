"""Service layer for meeting users"""
from sqlalchemy.orm import Session
from repositories import meeting_users as meeting_users_repository
from domain.schemas.meeting_users import MeetingUsersOutput, MeetingUsersInput
from domain.errors import ValidationError


def get_all_meeting_users(session: Session) -> list[MeetingUsersOutput]:
    """Get all registered meeting users"""
    return meeting_users_repository.get_all(session)


def create_meeting_user(session: Session, body: MeetingUsersInput) -> MeetingUsersOutput:
    """Create a new meeting user"""

    meeting_users = meeting_users_repository.add_meeting_user(session, body)

    # Check if exceeded the maximum capacity of the room of the meeting
    if len(meeting_users.users) > meeting_users.meeting.room.capacity:
        raise ValidationError("The room has exceeded its maximum capacity")

    return meeting_users
