"""Rooms service module."""
from sqlalchemy.orm import Session
from repositories import rooms as rooms_repository
from domain.schemas.rooms import RoomsInput
from domain.errors import ResourceAlreadyExists


def get_all_rooms(session: Session):
    """Get all registered rooms"""

    return rooms_repository.get_all(session)


def create_room(session: Session, body: RoomsInput):
    """Create a new room"""

    # Check if the room already exists by the key
    existing_room = rooms_repository.get_room_by_key(session, body.key)
    if existing_room:
        raise ResourceAlreadyExists(f"{existing_room.key}")

    return rooms_repository.add_room(session, body)
