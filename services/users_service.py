"""Users service module."""
from sqlalchemy.orm import Session
from repositories import users as users_repository
from domain.schemas.users import UsersInput
from domain.errors import ResourceAlreadyExists


def get_all_users(session: Session):
    """Get all registered users"""
    return users_repository.get_all(session)


def create_user(session: Session, body: UsersInput):
    """Create a new user"""

    # Check if user already exists by the key or name
    existing_user = users_repository.get_user_by_key_or_name(session, body.key, body.name)
    if existing_user:
        raise ResourceAlreadyExists(f"{existing_user.key} - {existing_user.name}")

    return users_repository.add_user(session, body)
