"""Controllers list"""
from . import users_controller
from . import rooms_controller
from . import meetings_controller
from . import meeting_users_controller

routes = [
    users_controller.router,
    rooms_controller.router,
    meetings_controller.router,
    meeting_users_controller.router
]

tags = [
    *users_controller.tags,
    *rooms_controller.tags,
    *meetings_controller.tags,
    *meeting_users_controller.tags
]
