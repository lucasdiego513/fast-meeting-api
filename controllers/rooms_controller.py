"""Rooms controller module."""
from fastapi import APIRouter, Depends, Body
from sqlalchemy.orm import Session
from domain.schemas.rooms import RoomsOutput, RoomsInput
from services import rooms_service
from config import get_session

router = APIRouter()

ROOMS = {
    "name": 'Rooms',
    "description": 'Manage rooms.'
}

tags = [ROOMS]


@router.post(
    "/rooms",
    summary="Register a new room to be available for meetings.",
    description="You can register a new room to be available for "
                "meetings by providing the room key, name, and capacity.",
    tags=[ROOMS['name']],
    response_model=RoomsOutput
)
def create_room(
        session: Session = Depends(get_session),
        body: RoomsInput = Body(description="Input data for creating a new room")) -> RoomsOutput:
    """Create a new room"""
    return rooms_service.create_room(session, body)


@router.get(
    "/rooms",
    summary="Get all available rooms",
    description="You can get information about all available rooms.",
    tags=[ROOMS['name']],
    response_model=list[RoomsOutput]
)
def get_all_rooms(session: Session = Depends(get_session)) -> list[RoomsOutput]:
    """Get all registered rooms"""
    return rooms_service.get_all_rooms(session)
