"""This module contains the controller for the meetings' endpoint."""
from fastapi import APIRouter, Depends, Body
from sqlalchemy.orm import Session
from domain.schemas.meetings import MeetingsOutput, MeetingsInput
from services import meetings_service
from config import get_session

router = APIRouter()

MEETINGS = {
    "name": 'Meetings',
    "description": 'Manage meetings.'
}

tags = [MEETINGS]


@router.post(
    "/meetings",
    summary="Create a new meeting",
    description=
    "You can create a new meeting by providing the meeting key, subject, "
    "description, and the date and time of the meeting. "
    "Furthermore, you also have to provide the meeting location by giving the room key.",
    tags=[MEETINGS['name']],
    response_model=MeetingsOutput
)
def create_meeting(
        session: Session = Depends(get_session),
        body: MeetingsInput = Body(description="Input data for creating a new meeting")) \
        -> MeetingsOutput:
    """Create a new meeting"""
    return meetings_service.create_meeting(session, body)


@router.get(
    "/meetings",
    summary="Get all registered meetings",
    description="You can get information about all registered meetings.",
    tags=[MEETINGS['name']],
    response_model=list[MeetingsOutput]
)
def get_all_meetings(session: Session = Depends(get_session)) -> list[MeetingsOutput]:
    """Get all registered meetings"""
    return meetings_service.get_all_meetings(session)
