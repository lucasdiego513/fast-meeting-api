"""Users controller module."""
from fastapi import APIRouter, Depends, Body
from sqlalchemy.orm import Session
from domain.schemas.users import UsersOutput, UsersInput
from services import users_service
from config import get_session

router = APIRouter()

USERS = {
    "name": 'Users',
    "description": 'Manage users.'
}

tags = [USERS]


@router.post(
    "/users",
    summary="Register a new user in the application",
    description=
    "You can register a new user in the application by providing the user key, name, and email."
    " This way, the user can be added to a meeting.",
    tags=[USERS['name']],
    response_model=UsersOutput
)
def create_user(
        session: Session = Depends(get_session),
        body: UsersInput = Body(description="Input data for creating a new user")) -> UsersOutput:
    """Create a new user"""
    return users_service.create_user(session, body)


@router.get(
    "/users",
    summary="Get all registered users",
    description="You can get information about all registered users.",
    tags=[USERS['name']],
    response_model=list[UsersOutput]
)
def get_all_users(session: Session = Depends(get_session)) -> list[UsersOutput]:
    """Get all registered users"""
    return users_service.get_all_users(session)
