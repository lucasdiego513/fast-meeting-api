"""Meeting Users Controller"""
from fastapi import APIRouter, Depends, Body
from sqlalchemy.orm import Session
from domain.schemas.meeting_users import MeetingUsersOutput, MeetingUsersInput
from services import meeting_users_service
from config import get_session

router = APIRouter()

MEETING_USERS = {
    "name": 'Meeting Users',
    "description": 'Manage meeting users.',
}

tags = [MEETING_USERS]


@router.post(
    "/meeting-users",
    summary="Register a new user to an existing meeting.",
    description="You can register a new user to an existing meeting given the "
                "meeting key and the user key.",
    tags=[MEETING_USERS['name']],
    response_model=MeetingUsersOutput
)
def create_meeting_user(
        session: Session = Depends(get_session),
        body: MeetingUsersInput = Body(description="Input data for creating a new meeting user")) \
        -> MeetingUsersOutput:
    """Create a new meeting user"""
    return meeting_users_service.create_meeting_user(session, body)


@router.get(
    "/meeting-users",
    summary="Get all registered meetings and the users subscribed to them.",
    description="Get information about all registered meetings and the"
                " users subscribed to each one of them.",
    tags=[MEETING_USERS['name']],
    response_model=list[MeetingUsersOutput]
)
def get_all_meeting_users(session: Session = Depends(get_session)) -> list[MeetingUsersOutput]:
    """Get all registered meeting users"""
    return meeting_users_service.get_all_meeting_users(session)
