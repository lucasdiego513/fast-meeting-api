"""Exporting all errors from the domain layer."""

from .generic import (
    group_errors,
    InternalError,
    ResourceNotFoundError,
    ResourceAlreadyExists,
    BadRequestError,
    ForbiddenRequestError,
    BadDatabaseConnection,
    MaxKeyLengthError,
    ValidationError
)

base_errors = {
    **group_errors(500, [
        InternalError(),
        MaxKeyLengthError('<<some-key>>')
    ]),
    **group_errors(404, [
        ResourceNotFoundError('<<some-resource>>')
    ]),
    **group_errors(400, [
        BadRequestError('<<some-resource>>'),
        ValidationError('<<some-resource>>')
    ]),
}
