# pylint: disable=too-few-public-methods
# pylint:disable=no-self-argument
"""Rooms schema"""
from pydantic import Field, field_validator
from domain.errors import MaxKeyLengthError
from .generic import TableSchema, GenericSchema


class RoomsInput(GenericSchema):
    """Rooms input schema"""
    key: str = Field(
        title='Room key',
        examples=["ROOM1"],
    )
    name: str = Field(
        title='Room name',
        examples=["Meeting Room 1"],
    )
    description: str = Field(
        title='Room description',
        examples=["Small meeting room"],
    )
    capacity: int = Field(
        title='Room capacity',
        examples=[10],
    )

    @field_validator("key")
    def key_upper(cls, value):
        """Make key upper case"""
        return value.upper()

    @field_validator("key")
    def key_format(cls, value):
        """Max key length is 5"""
        if len(value) > 5:
            raise MaxKeyLengthError(value)
        return value


class RoomsOutput(TableSchema):
    """Rooms output schema"""
    key: str = Field(
        title='Room key',
        examples=["ROOM1"],
    )
    name: str = Field(
        title='Room name',
        examples=["Meeting Room 1"],
    )
    description: str = Field(
        title='Room description',
        examples=["Small meeting room"],
    )
    capacity: int = Field(
        title='Room capacity',
        examples=[10],
    )
