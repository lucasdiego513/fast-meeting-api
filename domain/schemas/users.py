# pylint: disable=too-few-public-methods
# pylint: disable=no-self-argument
"""User schema"""
import re
from pydantic import Field, field_validator
from domain.errors import ValidationError, MaxKeyLengthError
from .generic import TableSchema, GenericSchema


class UsersInput(GenericSchema):
    """User input schema"""
    key: str = Field(
        title='User PETROBRAS key',
        description='Key of the user must be unique.',
        examples=["LUCD"],
    )
    name: str = Field(
        title='User name',
        description='User full name. Name of the user must be unique.',
        examples=["Lucas Diego de Freitas Lino"],
    )
    email: str = Field(
        title='User email',
        examples=["lucaslino@lccv.ufal.br"],
    )
    username: str = Field(
        title='User username',
        examples=["lucaslino"],
    )
    cpf: str = Field(
        title='User CPF',
        description='User CPF must follow the XXX.XXX.XXX-XX format.',
        examples=["123.456.789-01"],
    )
    phone: str = Field(
        title='User phone',
        examples=["82999999999"],
    )

    @field_validator("key")
    def key_upper(cls, value):
        """Make key upper case"""
        return value.upper()

    @field_validator("key")
    def key_format(cls, value):
        """Max key length is 5"""
        if len(value) > 5:
            raise MaxKeyLengthError(value)
        return value

    @field_validator("cpf")
    def cpf_format(cls, value):
        """Validate CPF format"""
        if not re.match(r'^\d{3}\.\d{3}\.\d{3}-\d{2}$', value):
            raise ValidationError("Invalid CPF format. Please use XXX.XXX.XXX-XX format. "
                                  "Example: 123.456.789-01")
        return value

    @field_validator("phone")
    def phone_format(cls, value):
        """Max phone length"""
        if len(value) > 11:
            raise ValidationError("Phone number must have a maximum of 11 characters.")
        # Check only numbers
        if not value.isdigit():
            raise ValidationError("Phone number must have only numbers.")
        return value


class UsersOutput(TableSchema):
    """User input schema"""
    key: str = Field(
        title='User PETROBRAS key',
        examples=["LUCD"],
    )
    name: str = Field(
        title='User name',
        examples=["Lucas Diego de Freitas Lino"],
    )
    email: str = Field(
        title='User email',
        examples=["lucaslino@lccv.ufal.br"],
    )
    username: str = Field(
        title='User username',
        examples=["lucaslino"],
    )
