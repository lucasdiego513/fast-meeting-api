# pylint: disable=too-few-public-methods
# pylint: disable=no-self-argument
"""Repository for rooms"""
from datetime import datetime
from pydantic import Field, field_validator, model_validator, FutureDatetime
from domain.errors import MaxKeyLengthError, ValidationError
from .generic import TableSchema, GenericSchema
from .rooms import RoomsOutput


class MeetingsInput(GenericSchema):
    """Meetings input schema"""
    key: str = Field(
        title='Meeting key',
        description='Key of the meeting must be unique.',
        examples=["MEET1"],
    )
    subject: str = Field(
        title='Meeting subject',
        description='You must provide a valid subject to create a meeting.',
        examples=["Team meeting"],
    )
    description: str = Field(
        title='Meeting description',
        examples=["Monthly team meeting"],
    )
    start: FutureDatetime = Field(
        title='Start datetime',
        description='You must provide a valid start datetime to create a meeting.',
        examples=["2222-01-01 10:00:00"],
    )
    end: FutureDatetime = Field(
        title='End datetime',
        description='You must provide a valid end datetime to create a meeting.',
        examples=["2222-01-01 11:00:00"],
    )
    room_key: str = Field(
        title='Room key',
        description='You must provide a valid room key to create a meeting.',
        examples=["ROOM1"],
    )

    @model_validator(mode='before')
    def check_dates(cls, values):
        """Check if end date is greater than start date"""
        start = datetime.strptime(values['start'], "%Y-%m-%d %H:%M:%S")
        end = datetime.strptime(values['end'], "%Y-%m-%d %H:%M:%S")
        if end <= start:
            raise ValidationError("End date must be greater than start date.")
        return values

    @field_validator("key")
    def key_upper(cls, value):
        """Make key upper case"""
        return value.upper()

    @field_validator("key", "room_key")
    def key_format(cls, value):
        """Max key length is 5"""
        if len(value) > 5:
            raise MaxKeyLengthError(value)
        return value

    @field_validator("subject")
    def subject_format(cls, value):
        """Cannot pass empty subject"""
        if not value:
            raise ValidationError("Subject cannot be empty.")
        return value


class MeetingsOutput(TableSchema):
    """Meetings output schema"""
    key: str = Field(
        title='Meeting key',
        examples=["MEET1"],
    )
    subject: str = Field(
        title='Meeting subject',
        examples=["Team meeting"],
    )
    description: str = Field(
        title='Meeting description',
        examples=["Monthly team meeting"],
    )
    start: FutureDatetime = Field(
        title='Start datetime',
        examples=["2222-01-01 10:00:00"],
    )
    end: FutureDatetime = Field(
        title='End datetime',
        examples=["2222-01-01 11:00:00"],
    )
    room: RoomsOutput
