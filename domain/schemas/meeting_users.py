"""Repository for rooms"""
# pylint: disable=no-self-argument
from pydantic import Field, field_validator
from domain.errors import MaxKeyLengthError
from .generic import TableSchema, GenericSchema
from .meetings import MeetingsOutput
from .users import UsersOutput


class MeetingUsersInput(GenericSchema):
    """Schema for creating or updating meeting users"""
    meeting_key: str = Field(
        title='Meeting key',
        description='Must be a valid meeting key.',
        example="MEET1",
    )
    user_key: str = Field(
        title='User key',
        description='Must be a valid user key.',
        example="LUCD",
    )

    @field_validator("meeting_key", "user_key")
    def meeting_key_upper(cls, value):
        """Make key upper case"""
        return value.upper()

    @field_validator("meeting_key", "user_key")
    def meeting_key_format(cls, value):
        """Max key length is 5"""
        if len(value) > 5:
            raise MaxKeyLengthError(value)
        return value


class MeetingUsersOutput(TableSchema):
    """Schema for retrieving meeting user information"""
    meeting: MeetingsOutput
    users: list[UsersOutput]
