# pylint: disable=too-few-public-methods
"""Model for Users"""
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
from domain.models.generic import GenericBase


class Users(GenericBase):
    """Users model"""
    __tablename__ = "users"

    key = Column("user_cd_users", String, primary_key=True)
    name = Column("user_nm_users", String)
    email = Column("user_email_users", String)
    username = Column("user_nm_username", String)
    cpf = Column("user_cd_cpf", String)
    phone = Column("user_nr_phone", String)
    meeting_users = relationship('MeetingUsers', back_populates='user')
