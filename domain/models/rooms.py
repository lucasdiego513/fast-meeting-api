# pylint: disable=too-few-public-methods
"""Model for Users"""
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship
from domain.models.generic import GenericBase


class Rooms(GenericBase):
    """Rooms model"""
    __tablename__ = "rooms"

    key = Column("room_cd_rooms", String, primary_key=True)
    name = Column("room_nm_rooms", String)
    description = Column("room_ds_rooms", String)
    capacity = Column("room_nr_capacity", Integer)

    meetings = relationship('Meetings', back_populates='room')
