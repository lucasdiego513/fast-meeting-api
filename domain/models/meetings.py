# pylint: disable=too-few-public-methods

"""Model for Users"""
from sqlalchemy import Column, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from domain.models.generic import GenericBase


class Meetings(GenericBase):
    """Meetings model"""
    __tablename__ = "meetings"

    key = Column("meet_cd_meetings", String, primary_key=True)
    subject = Column("meet_nm_subject", String)
    description = Column("meet_ds_meetings", String)
    start = Column("meet_df_start", DateTime)
    end = Column("meet_df_end", DateTime)
    room_key = Column("room_cd_rooms", String, ForeignKey('rooms.room_cd_rooms'))

    room = relationship('Rooms', back_populates='meetings')
    meeting_users = relationship('MeetingUsers', back_populates='meeting')
