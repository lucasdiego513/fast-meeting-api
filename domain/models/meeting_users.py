# pylint: disable=too-few-public-methods
"""Model for Users"""
from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.orm import relationship
from domain.models.generic import GenericBase


class MeetingUsers(GenericBase):
    """Meetings model"""
    __tablename__ = "meeting_users"

    meeting_key = Column(
        "meet_cd_meetings",
        String, ForeignKey("meetings.meet_cd_meetings"), primary_key=True)
    user_key = Column("user_cd_users", String, ForeignKey("users.user_cd_users"), primary_key=True)

    meeting = relationship("Meetings", back_populates="meeting_users")
    user = relationship("Users", back_populates="meeting_users")
