# Fast Meeting API backend

A SUBWEB API using Python.

- Main frameworks and packages:
    - [FastAPI](https://fastapi.tiangolo.com/)
    - [SQLAlchemy](https://www.sqlalchemy.org/) ([PostgreSQL](https://www.postgresql.org/) database).

## Install Python

Install [Python](https://www.python.org/downloads/) (version 3.10).

## Set and activate virtual environment

In project folder, execute the following commands:

```bash
pip install pipenv
export PIPENV_VENV_IN_PROJECT="enabled"
mkdir .venv
pipenv shell
source .venv/Scripts/activate
```

## Install required dependencies

You need to run the following installation command:

```bash
pipenv install --dev
```

For _user_ and _password_, use your development credentials.

## Set environment variables

Create a .env file with the required environment variables (use .env.example).

## Run server

On virtual environment, execute

```bash
pipenv run start
```

## Documentation

While running the server, one can access the [API documentation](http://localhost:4010/app/fast-meeting/api/docs).
